package sujet;

import java.io.IOException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class WebGrep {

	public static void main(String[] args) throws InterruptedException {
		// Initialize the program using the options given in argument
		if(args.length == 0) Tools.initialize(
			"-cT --threads=1000 Nantes https://fr.wikipedia.org/wiki/Nantes");
		else Tools.initialize(args);

		/*
		On initialise une liste qui nous permettra de stocker chaque 
		uri trouvée
		*/
		ConcurrentLinkedQueue<String> uris = new ConcurrentLinkedQueue<>(
			Tools.startingURL());
		/*
		On initialise une liste des pages contenants des résultats matchant 
		avec notre argument
		*/
		ConcurrentLinkedQueue<Tools.ParsedPage> pages = 
			new ConcurrentLinkedQueue<>();
		/*
		On initialise une liste contenant les pages à parser qui n'ont pas 
		encore été traitée
		*/
		BlockingDeque<Runnable> workQueue = new LinkedBlockingDeque<>();
		/*
		On initialise une liste contenant tout les threads actuellement en 
		cours d'execution
		*/
		BlockingDeque<Runnable> workInProgressQueue = 
			new LinkedBlockingDeque<>();

		//On prépare un service avec les paramètres spécifiés
		ExecutorService service = new ThreadPoolExecutor(Tools.numberThreads(),
			Tools.numberThreads(), 1, TimeUnit.SECONDS, workQueue);
	
		/*
		On définit une classe de runnable qui vont s'amuser à parser la page 
		à une adresse donnée
		*/
		class InspectRunnable implements Runnable {

			//L'adresse en question
			protected String address;

			//Un constructeur bidon
			public InspectRunnable(String address) {
				this.address = address;
			}

			//Le fonctionnement de notre parseur
			@Override
			public void run() {
				/*
				On indique que ce runnable s'éxecute en l'ajoutant à notre 
				liste de runnable en cours d'éxecution
				*/
				workInProgressQueue.add(this);
				//On log le début de l'éxecution
				System.out.println(String.format("Start inspection of %s", 
					address));
				try {
					//On parse effectivement
					Tools.ParsedPage page = Tools.parsePage(address);
					/*
					Si la page possède au moins une occurence du mot clé 
					cherché dans l'un de ses balises h1, h2, h3, h4, h5, h6, p,
					dd ou dt alors on l'ajoute à la liste de nos résultats.
					*/
					if(!page.matches().isEmpty())
						pages.add(page);
					//Si la page possède des redirection on les étudie
					if(!page.hrefs().isEmpty()) {
						for(String uri : page.hrefs()) {
							/*
							Si la redirection n'est pas connue et que son lien 
							contient le caractère spécifié alors on ajoute à la
							liste de tâche à effectuer.
							*/
							String[] uriProcess = uri.split("#");
							String exploreUri = uriProcess[0].split("?")[0];
							/*
							Ici on ne considère que la premère section de l'uri
							afin de ne pas explorer les uris de ce type :
							https://site.org#section1 mais uniquement celle de
							base à savoir : https://site.org.
							*/
							if ((!uris.contains(exploreUri))
								&& Pattern.compile(
									Tools.getRegularExpression(), 
									Pattern.DOTALL
								).matcher(exploreUri).find()) {
								//On log le lien trouvé
								System.out.println(String.format(
									"Found %s for inspection", exploreUri));
								uris.add(exploreUri);
								service.submit(new InspectRunnable(exploreUri));
							}
						}
					}
					//On log la fin de l'éxecution
					System.out.println(String.format("End of inspection of %s",
						address));
				} catch (IOException ioException) {
					//On log l'erreur survenue lors de l'éxecution
					System.err.println(String.format("IOException for %s", 
						address));
					/*
					On demande au service d'execution que la tâche est à
					réitérer.
					*/
					service.submit(new InspectRunnable(address));	
				}
				// On indique que la tâche n'est plus en cours
				workInProgressQueue.remove(this);
				/*
				Si plus aucune tâche n'est prévue et que cette tâche est la
				dernière du service, on demande l'arrêt de ce dernier.
				*/
				if (workInProgressQueue.size() + workQueue.size() == 0)
					service.shutdown();
			}
		}

		//On soumet à notre service les uris cibles de notre crawler
		for (String uri: Tools.startingURL())
			service.submit(new InspectRunnable(uri));
	
		//On attend la fin de l'éxecution de notre crawler
		while (!service.isTerminated()) {
			Thread.sleep(250);
		}

		//On affiche les résultats
		System.out.println("Résultats :");
		for (Tools.ParsedPage page : pages)
			Tools.print(page);
		//Puis nos sources
		System.out.println("Liens explorés :");
		for (String uri : uris)
			System.out.println(uri);
	}
}