# Rendu

## Préparatifs

### Initialisation

Non parallélisable on passe à la suite.

### Parsage

Pour chaque lien situé dans la liste à parser, on le parse dans un seul et unique `Thread`. Celui-ci place ses résultats dans une liste à traiter. Pour chaque élément dans cette fameuse liste n'importe quel `Thread` peut prendre l'un des éléments et le traiter. Si c'est un élément brut (`h*`, `d*` ou `p`), il le place dans les résultats. Sinon (si c'est un `a` par exemple), il le place dans la liste à parser et ainsi de suite jusqu'à ce que la liste de parsage et de "get" soient vide.

GET : URL -> HTML
PARSAGE : HTML -> TXT

Donc les méthodes parallélisables sont :

* La méthode de parsage
* La méthode de traitement des balises (celle de (h*, d* et p) et celle des a)

ParsedPage :

* Une adresse (url)
* Une liste de matchs (Donc qui ne renvoi nulle part)
* Une liste de pager à traiter

Liste de ParsedPage -> Quand on place une ParsedPage celle-ci possède que son adresse et on ne peut pas placer de nouvelle parsed page quand celle-ci est déjà présente

ParsedElement :

* Une adresse ou une ParsedPage Liée
* L'élement à traiter

Liste de `ParsedElement` -> Indique les éléments à Indentifier

### Sortie

Non parallélisable.

## Concrètement

Les `ParsedPage` ont été implémentées via un `Runnable` personnalisé nommé `InspectRunnable` qui à l'execution fouille la page web qu'il cible.
Afin de mémoriser les visites prévues, en cours et effectuées, nous avons instancié 4 différentes listes, la liste de "soumission" où l'on place les éléments à traiter, la liste de page en exploration, la liste des pages explorées et enfin la liste de toutes les urls explorées.
Suite à cela chaque `InspectRunnable` va explorer la page et va ajouter de nouveaux éléments à explorer s'il en rencontre.
Une fois le tout exploré, le programme affiche le résultat de son execution.
