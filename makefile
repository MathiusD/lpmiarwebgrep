default_target: run

clean:
	#On supprime le dossier abritant nos fichiers compilés
	@rm -rf bin

compile: clean
	#On créé le dossier de destination de nos fichiers compilés
	@mkdir bin
	#On compile la classe Tools qui est exploité par le programme
	@javac -cp libs/jsoup-1.12.1.jar src/sujet/Tools.java -d bin
	#On compile le programme
	@javac -cp bin:libs/jsoup-1.12.1.jar src/sujet/WebGrep.java -d bin

run: compile
	#Ici on lance le programme sans argument pour obtenir le comportement par défaut
	@java -cp bin:libs/jsoup-1.12.1.jar sujet.WebGrep -cT --threads=25 Nantes https://fr.wikipedia.org/wiki/Nantes